<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'JAGbtZBz57SZJ9XTWHieU2rLEtN/cX2xtPJo6Hx0VfgyW4eoK71mruiL2UeBy4gSQV+EyZJws3E2GZL2DvqHww==');
define('SECURE_AUTH_KEY',  '9E4tIlXRlHUIa9JfXAfKlXB456XDiWlQ05k085iVsk29uENJ5gcR9YAElHdpaU3T93FPhgnh7DKV0TQT3jeoCg==');
define('LOGGED_IN_KEY',    'JZL36KIFFQa6dUw95TOz4tPJawgDMLn9yRPILNrrbz9fD5FZFCAMPa4WLqspv3VQqzBCvQb0o3kKvjrP9UB0Jw==');
define('NONCE_KEY',        'UkX+CQAPyMmGz4jrRgWSbmsyIGLUEAUx7VIlYmGEBIXsmypL1nEjALpsYzE7KFxez4iodO4AET9y8O1xuqq1uA==');
define('AUTH_SALT',        'KwKoi3UJvK36VoDis/LDEFIRG9o+wnPBJoXbQaDd5UHC72RQVis3RUgagL+DIegG5/DWjGyuSDa3dq652YPJVw==');
define('SECURE_AUTH_SALT', 'uyn6EWZpH6cIu8iasxDRqwyuqmrfL8VKqzFQ39MJP9p/yA3fBblAapjZSosOEV+GU1hfiDf+WWgRB/yJyXP/cQ==');
define('LOGGED_IN_SALT',   'RsJrzd69alGdsPbZVSJawExK6Pge53RdJwN9LxzwnQoTzAow9Ti/jvfZSEF/TuByAo/Q3u8zmnYVREdZ4j57dg==');
define('NONCE_SALT',       'gydrQ5GNozKttVUZBwcjVABSicz3OaOScosDZV/BwGqVhNfd9bpxwau7b2434+6kx331+xPKe9Cgxvl7l7i8qw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
